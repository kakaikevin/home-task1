package ee.kitchen.deliveryservice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import ee.cleankitchen.deliveryservice.DeliveryServiceClient;
import ee.cleankitchen.orderservice.DefaultOrderServiceClient;

class DeliveryServiceClientTest {


  @Test
  void testDeliveryNotSupportedForWeekends() {
    DefaultOrderServiceClient orderServiceClient = new DefaultOrderServiceClient();
    DeliveryServiceClient deliveryServiceClient = new DeliveryServiceClient(orderServiceClient);
    // Saturday
    LocalDate deliveryDate = LocalDate.of(2021, 12, 12);
    assertTrue(deliveryServiceClient.availableDeliveryTimes(deliveryDate).isEmpty());
  }

  @Test
  void testWhenNoOrdersScheduled() {
    DefaultOrderServiceClient orderServiceClient = new DefaultOrderServiceClient();
    DeliveryServiceClient deliveryServiceClient = new DeliveryServiceClient(orderServiceClient);
    LocalDate deliveryDate = LocalDate.of(2021, 12, 13);
    List<LocalTime> allowedDeliveryTimes =
        deliveryServiceClient.availableDeliveryTimes(deliveryDate);
    // currently supports 3 delivery times
    assertEquals(3, allowedDeliveryTimes.size());
    assertTrue(allowedDeliveryTimes.contains(LocalTime.of(10, 30)));
    assertTrue(allowedDeliveryTimes.contains(LocalTime.of(12, 30)));
    assertTrue(allowedDeliveryTimes.contains(LocalTime.of(18, 30)));
  }

  @Test
  void testWhenTwoOrdersScheduledForTuesday() {
    DefaultOrderServiceClient orderServiceClient = new DefaultOrderServiceClient();
    LocalDate deliveryDate = LocalDate.of(2021, 12, 07);
    orderServiceClient.scheduleOrder(deliveryDate, Map.of("date", deliveryDate, "time",
        LocalTime.of(10, 30), "orderId", "1", "customerId", "a-111"));
    orderServiceClient.scheduleOrder(deliveryDate, Map.of("date", deliveryDate, "time",
        LocalTime.of(10, 30), "orderId", "2", "customerId", "a-112"));
    DeliveryServiceClient deliveryServiceClient = new DeliveryServiceClient(orderServiceClient);
    List<LocalTime> allowedDeliveryTimes =
        deliveryServiceClient.availableDeliveryTimes(deliveryDate);
    assertEquals(2, allowedDeliveryTimes.size());
    assertTrue(allowedDeliveryTimes.contains(LocalTime.of(12, 30)));
    assertTrue(allowedDeliveryTimes.contains(LocalTime.of(18, 30)));
  }

  @Test
  void testWhenAllDeliveryTimesAreFull() {
    DefaultOrderServiceClient orderServiceClient = new DefaultOrderServiceClient();
    LocalDate deliveryDate = LocalDate.of(2021, 12, 07);
    orderServiceClient.scheduleOrder(deliveryDate, Map.of("date", deliveryDate, "time",
        LocalTime.of(10, 30), "orderId", "1", "customerId", "a-111"));
    orderServiceClient.scheduleOrder(deliveryDate, Map.of("date", deliveryDate, "time",
        LocalTime.of(10, 30), "orderId", "2", "customerId", "a-112"));
    orderServiceClient.scheduleOrder(deliveryDate, Map.of("date", deliveryDate, "time",
        LocalTime.of(12, 30), "orderId", "3", "customerId", "a-113"));
    orderServiceClient.scheduleOrder(deliveryDate, Map.of("date", deliveryDate, "time",
        LocalTime.of(12, 30), "orderId", "4", "customerId", "a-114"));
    orderServiceClient.scheduleOrder(deliveryDate, Map.of("date", deliveryDate, "time",
        LocalTime.of(18, 30), "orderId", "5", "customerId", "a-115"));
    orderServiceClient.scheduleOrder(deliveryDate, Map.of("date", deliveryDate, "time",
        LocalTime.of(18, 30), "orderId", "6", "customerId", "a-116"));
    DeliveryServiceClient deliveryServiceClient = new DeliveryServiceClient(orderServiceClient);
    assertTrue(deliveryServiceClient.availableDeliveryTimes(deliveryDate).isEmpty());
  }

  @Test
  void testWhenTwoOrdersScheduledForMonday() {
    DefaultOrderServiceClient orderServiceClient = new DefaultOrderServiceClient();
    LocalDate deliveryDate = LocalDate.of(2021, 12, 06);
    orderServiceClient.scheduleOrder(deliveryDate, Map.of("date", deliveryDate, "time",
        LocalTime.of(10, 30), "orderId", "1", "customerId", "a-111"));
    orderServiceClient.scheduleOrder(deliveryDate, Map.of("date", deliveryDate, "time",
        LocalTime.of(10, 30), "orderId", "2", "customerId", "a-112"));
    DeliveryServiceClient deliveryServiceClient = new DeliveryServiceClient(orderServiceClient);
    List<LocalTime> allowedDeliveryTimes =
        deliveryServiceClient.availableDeliveryTimes(deliveryDate);
    assertEquals(3, allowedDeliveryTimes.size());
    assertTrue(allowedDeliveryTimes.contains(LocalTime.of(10, 30)));
    assertTrue(allowedDeliveryTimes.contains(LocalTime.of(12, 30)));
    assertTrue(allowedDeliveryTimes.contains(LocalTime.of(18, 30)));
  }
}
