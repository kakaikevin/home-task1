package ee.cleankitchen.deliveryservice;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import ee.cleankitchen.orderservice.OrderServiceClient;

public class DeliveryServiceClient implements DeliveryService {

  private OrderServiceClient orderServiceClient;

  public DeliveryServiceClient(OrderServiceClient orderServiceClient) {
    this.orderServiceClient = orderServiceClient;
  }

  @Override
  public synchronized List<LocalTime> availableDeliveryTimes(LocalDate deliveryDate) {

    // get orders already scheduled for the given date
    Map<LocalTime, Long> ordersPerDeliveryTime = getScheduledOrders(deliveryDate).stream()
        .collect(Collectors.groupingBy(map -> (LocalTime) map.get("time"), Collectors.counting()));
    // Get number of orders allowed per delivery time
    int numberOfOrdersAllowed =
        DeliveryDayOfWeek.getNumberOfOrdersAllowedPerDeliveryTime(deliveryDate);
    if (numberOfOrdersAllowed == 0) {
      return Collections.emptyList();
    }
    List<LocalTime> availableDeliveryTimes = new ArrayList<>();
    for (LocalTime supportedDeliveryTime : allowedDeliveryTimes()) {
      if (!ordersPerDeliveryTime.containsKey(supportedDeliveryTime)
          || ordersPerDeliveryTime.get(supportedDeliveryTime) < numberOfOrdersAllowed) {
        availableDeliveryTimes.add(supportedDeliveryTime);
      }

    }
    return availableDeliveryTimes;
  }

  private List<Map<String, Object>> getScheduledOrders(LocalDate deliveryDate) {
    List<Map<String, Object>> scheduledOrders = orderServiceClient.getBy(deliveryDate);
    return scheduledOrders == null ? Collections.emptyList() : scheduledOrders;
  }
}
