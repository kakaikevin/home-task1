package ee.cleankitchen.deliveryservice;

import java.time.LocalDate;

public enum DeliveryDayOfWeek {

  MONDAY(4), TUESDAY(2), WEDNESDAY(2), THURSDAY(2), FRIDAY(2), SATURDAY(0), SUNDAY(0);

  private int numberOfOrdersAllowedPerDeliveryTime;

  private DeliveryDayOfWeek(int numberOfOrdersAllowedPerDeliveryTime) {
    this.numberOfOrdersAllowedPerDeliveryTime = numberOfOrdersAllowedPerDeliveryTime;
  }

  public int getNumberOfOrdersAllowedPerDeliveryTime() {
    return numberOfOrdersAllowedPerDeliveryTime;
  }

  public static int getNumberOfOrdersAllowedPerDeliveryTime(LocalDate date) {
    DeliveryDayOfWeek dayOfWeek = getDeliveryDayOfWeek(date);
    return dayOfWeek == null ? 0 : dayOfWeek.numberOfOrdersAllowedPerDeliveryTime;
  }

  private static DeliveryDayOfWeek getDeliveryDayOfWeek(LocalDate date) {

    switch (date.getDayOfWeek()) {

      case MONDAY:
        return MONDAY;
      case TUESDAY:
        return TUESDAY;
      case WEDNESDAY:
        return WEDNESDAY;
      case THURSDAY:
        return THURSDAY;
      case FRIDAY:
        return FRIDAY;
      case SATURDAY:
        return SATURDAY;
      case SUNDAY:
        return SUNDAY;
      default:
        return null;
    }

  }

}
