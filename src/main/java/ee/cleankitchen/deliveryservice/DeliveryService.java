package ee.cleankitchen.deliveryservice;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public interface DeliveryService {

  /**
   * Return list of times delivery times available for a given {@code LocalDate}.
   * 
   * An example response looks like this:
   * 
   * <pre>
   * {@code
   *  List.of(LocalTime.of(10, 30), LocalTime.of(12, 30), LocalTime.of(18, 30))
   * }
   * </pre>
   */
  List<LocalTime> availableDeliveryTimes(LocalDate deliveryDate);

  // currently supported delivery times
  default List<LocalTime> allowedDeliveryTimes() {
    return List.of(LocalTime.of(10, 30), LocalTime.of(12, 30), LocalTime.of(18, 30));
  }
}
