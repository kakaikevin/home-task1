package ee.cleankitchen.orderservice;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * Default implementation of order service client to help with testing
 */
public class DefaultOrderServiceClient implements OrderServiceClient {

  Map<LocalDate, List<Map<String, Object>>> scheduledOrders = new HashMap<>();

  @Override
  public List<Map<String, Object>> getBy(LocalDate date) {
    return scheduledOrders.get(date);
  }

  public void scheduleOrder(LocalDate date, Map<String, Object> order) {
    List<Map<String, Object>> ordersForTheDay =
        scheduledOrders.getOrDefault(date, new ArrayList<>());
    ordersForTheDay.add(order);
    scheduledOrders.put(date, ordersForTheDay);
  }
}
